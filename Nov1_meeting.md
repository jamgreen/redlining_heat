---
output: pdf
title: 01-11-2019 Meeting with Vivek
fontsize: 11pt
fontfamily: garamond
---

1. Have we seen greater or less exposure to traditionally marginalized communities due to historic redlining? (Pull LTDB data back to 1970)
	A. Pull income
	B. Age (over 65)
	C. Race  
2.  Satellite Imaging
    A.  MODIS (100m cells)  
3. Identify tracts that have significantly changed between 1970 and 2017 in the 20 cities where they've collected ambient temperature (using afternoon temperatures)
    A.  Map changes in neighborhoods in the hottest areas of the city
    B. Do centroid intersection of tracts to redlined boundaries...pull D and A redlined areas and look at those changes over time (suspect A areas are probably more stable)     	