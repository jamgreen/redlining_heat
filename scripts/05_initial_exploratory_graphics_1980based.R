#xploratory graphics using 1990 data

if(!require(pacman)){install.packages("pacman"); library(pacman)}
p_load(tidyverse, sf, data.table, ggthemes)


# holc_ltdb <- st_read("data/01_31_holc_ltdb_combined_1990_2012.shp")
holc_ltdb <- st_read("data/11_21_holc_ltdb_combined_1980_2012.shp")

# holc_ltdb <- holc_ltdb %>%
#   mutate(pop_change = (pop10 - pop90)/pop90,
#          white_change = (nhwht10 - nhwht90)/nhwht90,
#          black_change = (nhblk10 - nhblk90)/nhblk90,
#          hisp_change = (hisp10 - hisp90)/hisp90,
#          vacancy_90 = vac90/hu90,
#          vacancy_10 = vac10/hu10,
#          povrate_90 = npov90/dpov90,
#          povrate_12 = npov12/dpov12)

holc_ltdb <- holc_ltdb %>%
  mutate(pop_change = (pop10 - pop80)/pop80,
         white_change = (nhwht10 - nhwht80)/nhwht80,
         black_change = (nhblk10 - nhblk80)/nhblk80,
         hisp_change = (hisp10 - hisp80)/hisp80,
         vacancy_80 = vac80/hu80,
         vacancy_10 = vac10/hu10,
         povrate_80 = npov80/dpov80,
         povrate_12 = npov12/dpov12)


holc_city_pop <- holc_ltdb %>% 
  group_by(city_name, holc_grade) %>% 
  summarise(avg_pop_change = mean(pop_change,  na.rm = TRUE),
            avg_white_change = mean(white_change, na.rm = TRUE),
            avg_black_change = mean(black_change, na.rm = TRUE),
            avg_hisp_change = mean(hisp_change, na.rm = TRUE)) %>% 
  data.frame(.) %>% 
  select(-geometry)

city_tot_pop_plot <- ggplot(holc_city_pop, aes(x = holc_grade, y = avg_pop_change)) +
  geom_col() +
  facet_wrap(~city_name, scales = "free") +
  theme_minimal()

holc_tall_pop <-  ggplot(holc_city_pop, aes(x = city_name, y = avg_pop_change, 
                                            group = holc_grade, fill = holc_grade)) +
  geom_col(position = "stack") +
  theme_minimal() + scale_y_continuous(labels = scales::percent) +
  labs(x = "City", y = "Average Pop. Change", fill = "HOLC Grade")  +
  coord_flip()

holc_tall_pop2 <-  ggplot(holc_city_pop, aes(x = city_name, y = avg_pop_change, 
                                            group = holc_grade, fill = holc_grade)) +
  geom_col(position = "fill") +
  theme_minimal() + scale_y_continuous(labels = scales::percent) +
  labs(x = "City", y = "Average Pop. Change", fill = "HOLC Grade")  
# holc_mhi <- holc_ltdb %>% 
#   group_by(city_name, holc_grade)


#get stacked barchart of pop change proportions--------

# holc_city <- holc_ltdb %>% 
# group_by(city_name, city_geoid)  %>% 
#   mutate(pop_change = (pop10 - pop90)/pop90,
#             white_change = (nhwht10 - nhwht90)/nhwht90,
#             black_change = (nhblk10 - nhblk90)/nhblk90,
#             hisp_change = (hisp10 - hisp90)/hisp90,
#             vacancy_90 = vac90/hu90,
#             vacancy_10 = vac10/hu10,
#             povrate_90 = npov90/dpov90,
#             povrate_12 = npov12/dpov12)

holc_ltdb <- data.table(holc_ltdb)

# calculate populations by city, by holc grade

holc_city <- holc_ltdb[, .(pop80 = sum(pop80),
                           pop10 = sum(pop10),
                           white80 = sum(nhwht80),
                           white10 = sum(nhwht10),
                           black80 = sum(nhblk80),
                           black10 = sum(nhblk10),
                           hisp80 = sum(hisp80),
                           hisp10 = sum(hisp10),
                           npov80_2 = sum(npov80, na.rm = TRUE),
                           dpov80_2 = sum(dpov80, na.rm = TRUE),
                           npov12_2 = sum(npov12, na.rm = TRUE),
                           dpov12_2 = sum(dpov12, na.rm = TRUE)), by = .(city_name, holc_id)]

holc_city[, ':='(pop_change = (pop80 - pop10)/pop80,
                 white_change_per = (white10 - white80)/white80,
                 black_change_per = (black10 - black80)/black80,
                 hisp_change_per = (hisp10 - hisp80)/hisp80,
                 povrate80 = npov80_2/dpov80_2,
                 povrate12 = npov12_2/dpov12_2,
                 white_change_diff = white10 - white80,
                 black_change_diff = black10 - black80,
                 hisp_change_diff = hisp10 - hisp80)]

holc_city[, ':='(holc_tot_pop80 = sum(pop80),
                 holc_tot_pop10 = sum(pop10),
                 holc_white_pop80 = sum(white80),
                 holc_white_pop10 = sum(white10),
                 holc_black_pop80 = sum(black80),
                 holc_black_pop10 = sum(black10),
                 holc_hisp_pop80 = sum(hisp80),
                 holc_hisp_pop10 = sum(hisp10),
                 holc_npov80 = sum(npov80_2, na.rm = TRUE),
                 holc_dpov80 = sum(dpov80_2, na.rm = TRUE),
                 holc_npov12 = sum(npov12_2, na.rm = TRUE),
                 holc_dpov12 = sum(dpov12_2, na.rm = TRUE)), by = city_name]

holc_city[, ':=' (pop_prop80 = pop80/holc_tot_pop80,
                  pop_prop10 = pop10/holc_tot_pop10,
                  white_prop80 = white80/holc_white_pop80,
                  white_prop10 = white10/holc_white_pop10,
                  black_prop80 = black80/holc_black_pop80,
                  black_prop10 = black10/holc_black_pop10,
                  hisp_prop80 = hisp80/holc_hisp_pop80,
                  hisp_prop10 = hisp10/holc_hisp_pop10,
                  npov_prop80 = npov80_2/holc_npov80,
                  dpov_prop80 = dpov80_2/holc_dpov80,
                  npov_prop12 = npov12_2/holc_npov12,
                  dpov_prop12 = dpov12_2/holc_dpov12)]

#tot_pop proportional fig

city_prop_pop <-  ggplot(holc_city, aes(x = city_name, y = pop_prop80, group = holc_id, fill = holc_id)) +
  geom_col(position = "stack") +
  theme_minimal() + scale_y_continuous(labels = scales::percent) +
  labs(x = "City", y = "Average Pop. Change", fill = "HOLC Grade")  +
  coord_flip()
